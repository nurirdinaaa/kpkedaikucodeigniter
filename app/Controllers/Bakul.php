<?php

namespace App\Controllers;

class Bakul extends BaseController
{
    function __construct()
    {
        $this->session = session();
        $this->produk_model = new \App\Models\ProdukModel();
    }

    //disply items dalam bakul
    public function index()
    {

        // $_SESSION['cart'] = [
        //     'items' => [
        //         [ 'id' => 1, 'nama' => 'Lastik', 'harga' => '10.99', 'qty' => 2 ],
        //         [ 'id' => 2, 'nama' => 'Barbie Doll', 'harga' => '15.95', 'qty' => 3 ],
        //         [ 'id' => 3, 'nama' => 'RC Car', 'harga' => '13.99', 'qty' => 1 ],
        //         [ 'id' => 4, 'nama' => 'Toy Gun', 'harga' => '24.45', 'qty' => 4 ]
        //     ]
        // ];

        return view('bakul', $this->data);
    }

    //add to cart
    function add()
    {
        //$this->request->getPost('nama')
        // dd($_POST);

        //get data from form
        $produk_id = $this->request->getPost('produk_id');
        $qty = $this->request->getPost('qty');

        //find produk from database
        $produk = $this->produk_model->find($produk_id);

        //if product found, add to cart
        if ($produk) {
            $this->add_cart($produk['id'], $produk['nama'], $produk['harga'], $qty);
            $_SESSION['success'] = true;
            $this->session->markAsFlashdata('success');
        }
        return redirect()->back();
    }

    function remove($id)
    {
        foreach ($_SESSION['cart']['items'] as $k => $item) {
            if ($item['id'] == $id) {
                unset($_SESSION['cart']['items'][$k]);
            }
        }

        $_SESSION['success'] = true;
        $this->session->markAsFlashdata('success');
        return redirect()->back();
    }

    function update()
    {
        // dd($_POST);

        //reset item dalam cart
        $_SESSION['cart'] = [
            'items' => []
        ];

        $qty = $this->request->getPost('qty');
        // $all_ids = [];
        // foreach ($qty as $id => $val) {
        //     $all_ids[] = $id;
        // } CARA LAIN DIBAWAH:

        $all_ids = array_keys($qty);

        $produks = $this->produk_model->find($all_ids);

        foreach ($produks as $produk) {
            if ($qty[$produk['id']] > 0) {
                $this->add_cart($produk['id'], $produk['nama'], $produk['harga'], $qty[$produk['id']]);
            }
        }

        $_SESSION['success'] = true;
        $this->session->markAsFlashdata('success');
        return redirect()->back();
    }

    //internal function -- add items to session (cart)
    protected function add_cart($id, $nama, $harga, $qty)
    {
        //isset utk check dah pernah ada ke belum, kalau belum, SET
        if (!isset($_SESSION['cart']['items'])) {
            $_SESSION['cart'] = [
                'items' => []
            ];
        }

        //kalau dah ada item tambah qty
        $found = false;
        foreach ($_SESSION['cart']['items'] as $index => $item) {
            if ($item['id'] == $id) {
                $_SESSION['cart']['items'][$index]['qty'] += $qty;
                $found = true;
            }
        }

        //kalau belum ada item, create baru
        if (!$found) {
            $_SESSION['cart']['items'][] = [
                'id' => $id,
                'nama' => $nama,
                'harga' => $harga,
                'qty' => $qty
            ];
        }
        return true;
    }
}
