<?= $this->extend('templates/front_layout') ?>

<?= $this->section('main-content') ?>

<div class="row">
    <div class="col-12">
        <h2><a href="/" class="btn ntn-sm btn-primary">Back</a>
            Your shopping cart</h2>
    </div>
</div>
<?php if (isset($_SESSION['success'])) : ?>
    <div class="row">
        <div class="col">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Cart updated!</strong> Quantity and items have been updated
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>

<?php endif; ?>
<div class="row">
    <div class="col-12">
        <form action="/bakul/update" method="POST">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>

                        </th>
                        <th>

                        </th>
                        <th>
                            Product
                        </th>
                        <th>
                            Price
                        </th>
                        <th width="15%">
                            Qty
                        </th>
                        <th>
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody>

                    <?php if (isset($_SESSION['cart']) && (count($_SESSION['cart']['items']) > 0)) : ?>
                        <?php $counter = 0; ?>
                        <?php $total_amount = 0; ?>
                        <?php foreach ($_SESSION['cart']['items'] as $item) : ?>
                            <tr>
                                <td><?= ++$counter; ?></td>
                                <td><a href="#" class="btn btn-danger btn-sm" onclick="confirm_remove(<?= $item['id']; ?>)">REMOVE</a> </td>
                                <td><?= $item['nama']; ?></td>
                                <td><?= number_format($item['harga'], 2); ?></td>
                                <td><input type="number" step="1" name="qty[<?= $item['id'] ?>]" value="<?= $item['qty']; ?>" class="form-control"> </td>
                                <td>RM <?= number_format($item['harga'] * $item['qty'], 2); ?></td>
                            </tr>
                            <?php $total_amount += ($item['harga'] * $item['qty']); ?>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="5" align="right"><strong>Total Amount</strong> </td>
                            <td>RM <?= number_format($total_amount, 2); ?></td>
                        </tr>

                    <?php else : ?>
                        <tr>
                            <td colspan="6" class="text-center">Your cart is empty</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>

            <a href="/checkout" class="btn btn-primary float-right">checkout</a>
            <button class="btn btn-info mr-1 float-right">update cart</button>
        </form>
    </div>
</div>
<script>
    function confirm_remove(id) {
        if (confirm('Are you sure you want to remove this produk from cart?')) {
            window.location.href = '/bakul/remove/' + id;
        }
    }
</script>
<?= $this->endSection() ?>